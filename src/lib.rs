use serde_json::Value;
use ureq::{Request, Agent, AgentBuilder, MiddlewareNext, Response};
#[derive(Clone, Debug)]
pub struct GTInstance {
    source: String,
    target: String,
    agent: Agent,
}
#[derive(Debug)]
pub enum GTError {
    NoResults(String),
    UReqError(ureq::Error),
}

impl From<ureq::Error> for GTError {
    fn from(e: ureq::Error) -> GTError {
        GTError::UReqError(e)
    }
}

fn set_referer(req: Request, next: MiddlewareNext) -> Result<Response, ureq::Error> {
    next.handle(req.set("Referer", "http://translate.google.com"))
}


impl GTInstance {
    pub fn new(source_lang: impl Into<String>, target_lang: impl Into<String>) -> Self {
        let source = source_lang.into();
        let target = target_lang.into();
        let agent = AgentBuilder::new().middleware(set_referer).user_agent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36").build();
        Self {
            source: source,
            target: target,
            agent: agent,
        }
    }
    fn package_rpc(&self, text: String) -> String {
        let parameter = serde_json::json!([
            [text, self.source, self.target, true],
            [1]
        ]);
        let escaped_parameter = parameter.to_string();
        let rpc = serde_json::json!([[[
            "MkEWBc", escaped_parameter, Value::Null, "generic"
        ]]]);
        //let final_result = format!("f.req={}&", urlencoding::encode(&rpc.to_string()));
        //final_result
        rpc.to_string()
    }
    pub fn translate(&self, text: impl Into<String>) -> Result<String, GTError> {
        let input_text = text.into();
        //let mut translated = String::new();
        let lines = self.agent.post("https://translate.google.com/_/TranslateWebserverUi/data/batchexecute").send_form(&[("f.req", &self.package_rpc(input_text))])?.into_string().unwrap();
        for line in lines.lines() {
            if line.contains("MkEWBc") {
                let value_resp: Value = serde_json::from_str(line).unwrap();
                let good_value: Value = serde_json::from_str(value_resp[0][2].as_str().unwrap()).unwrap();
                return Ok(good_value[1][0][0][5].as_array().unwrap().iter().map(|x| x[0].as_str().unwrap()).collect::<Vec<&str>>().join(" "));
            }
        }
        Err(GTError::NoResults(String::from("No results")))
    }
}